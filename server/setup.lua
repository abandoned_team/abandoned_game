gra = love.graphics
img = love.image
key = love.keyboard
tim = love.timer
eve = love.event
win = love.window

-- setup of asset directories
assetsDir = "assets"
animationDir = assetsDir.."/animations"
soundsDir = assetsDir.."/sounds"
musicDir = assetsDir.."/music"
